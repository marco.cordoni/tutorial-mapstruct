# Introduction

This project is a tutorial on how to use [MapStruct](https://mapstruct.org/) in a [Quarkus](https://quarkus.io/) project.

# Dependencies

## POM Dependencies

```xml
<!-- mapstruct dependencies -->
<dependency>
    <groupId>org.mapstruct</groupId>
    <artifactId>mapstruct</artifactId>
    <version>1.5.5.Final</version>
</dependency>
<dependency>
    <groupId>org.mapstruct</groupId>
    <artifactId>mapstruct-processor</artifactId>
    <version>1.5.5.Final</version>
    <scope>provided</scope>
</dependency>
```

# What, why and how?

## What is MapStruct?

MapStruct is a code generator that greatly simplifies the implementation of mappings between Java types.

The generated mapping code uses plain method invocations and thus is fast, type-safe and easy to understand.

## Why use MapStruct?

Multi-layered applications often require to map between different object models (e.g. entities and DTOs). 
Writing such mapping code is a tedious and error-prone task. 
MapStruct aims at simplifying this work by automating it as much as possible.

In contrast to other mapping frameworks MapStruct generates bean mappings at compile-time which ensures a high performance, allows for fast developer feedback and thorough error checking.

## How does MapStruct work?

MapStruct is an annotation processor which is plugged into the Java compiler and can be used in command-line builds (Maven, Gradle etc.) as well as from within your preferred IDE.

MapStruct uses sensible defaults but steps out of your way when it comes to configuring or implementing special behavior.

# Integration with Lombok

Both MapStruct and Lombok generate code at compile-time, so you need to force the building phase to generate Lombok code first and then MapStruct code.
If this step is not performed correctly, MapStruct will have to generate mappers using objects without getters and setters and therefore these methods will not have a correct implementation.

To solve this problem we need the `lombok-mapstruct-binding` library:

```xml
<!-- lombok -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.28</version>
    <scope>provided</scope>
</dependency>

<!-- to make lombok and mapstruct work together -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok-mapstruct-binding</artifactId>
    <version>0.2.0</version>
</dependency>
```

And we need to define the `maven-compiler-plugin` as follows:

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-compiler-plugin</artifactId>
    <version>3.11.0</version>
    <configuration>
        <source>maven.compiler.source</source>
        <target>maven.compiler.target</target>
        <annotationProcessorPaths>
            <path>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>1.18.28</version>
            </path>
            <path>
                <groupId>io.quarkus</groupId>
                <artifactId>quarkus-extension-processor</artifactId>
                <version>3.3.2</version>
            </path>
            <path>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok-mapstruct-binding</artifactId>
                <version>0.2.0</version>
            </path>
            <path>
                <groupId>org.mapstruct</groupId>
                <artifactId>mapstruct-processor</artifactId>
                <version>1.5.5.Final</version>
            </path>
        </annotationProcessorPaths>
    </configuration>
</plugin>
```

# Documentation 

[Here](https://mapstruct.org/documentation/stable/reference/html/) you can find the documentation of the latest version.