package repository;

import entity.AuthorEntity;
import entity.BookEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class AuthorRepository {

    public List<AuthorEntity> listAll() {
        List<AuthorEntity> authorEntityList = new ArrayList<>();

        List<BookEntity> bookEntityList1 = new ArrayList<>();
        BookEntity bookEntity1 = new BookEntity(
                1,
                "Ready Player One",
                null,
                LocalDate.of(2011, 9, 16)
        );
        bookEntityList1.add(bookEntity1);

        AuthorEntity authorEntity1 = new AuthorEntity(
                "Ernest Cline",
                null,
                LocalDate.of(1972, 3, 29),
                bookEntityList1
        );
        authorEntityList.add(authorEntity1);
        bookEntity1.setAuthor(authorEntity1);

        List<BookEntity> bookEntityList2 = new ArrayList<>();
        BookEntity bookEntity2 = new BookEntity(
                2,
                "La profezia dell'armadillo",
                null,
                LocalDate.of(2011, 9, 16)
        );
        bookEntityList2.add(bookEntity2);

        AuthorEntity authorEntity2 = new AuthorEntity(
                "Michele Reich",
                "Zerocalcare",
                LocalDate.of(1983, 12, 12),
                bookEntityList2
        );
        authorEntityList.add(authorEntity2);
        bookEntity2.setAuthor(authorEntity2);

        return authorEntityList;
    }
}
