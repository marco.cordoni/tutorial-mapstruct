package repository;

import entity.AuthorEntity;
import entity.BookEntity;
import jakarta.enterprise.context.ApplicationScoped;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@ApplicationScoped
public class BookRepository {

    public List<BookEntity> listAll() {
        return DataMocked.listAllMockedBooks();
    }

    public BookEntity persist(BookEntity bookEntity) {
        bookEntity.setId(new Random().nextInt(100, 100000));
        return bookEntity;
    }
}
