package repository;

import entity.AuthorEntity;
import entity.BookEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class DataMocked {

    private static List<BookEntity> bookEntityList;
    private static List<AuthorEntity> authorEntityList;

    private static void setUp() {
        bookEntityList = new ArrayList<>();
        authorEntityList = new ArrayList<>();

        BookEntity bookEntity1_1 = new BookEntity(1, "Ready Player One", null, LocalDate.of(2011, 9, 16));
        bookEntity1_1.setCreatedAt( LocalDateTime.of(2018, Month.JULY, 29, 19, 30, 40));
        List<BookEntity> bookEntityList1 = new ArrayList<>();
        bookEntityList1.add(bookEntity1_1);
        AuthorEntity authorEntity1 = new AuthorEntity("Ernest Cline", null, LocalDate.of(1972, 3, 29), bookEntityList1);
        authorEntity1.setCreatedAt( LocalDateTime.of(2016, Month.JULY, 29, 19, 30, 40));
        bookEntity1_1.setAuthor(authorEntity1);

        BookEntity bookEntity2_1 = new BookEntity(2, "Scheletri", null, LocalDate.of(2018, 9, 16));
        bookEntity2_1.setCreatedAt( LocalDateTime.of(2021, Month.JULY, 29, 19, 30, 40));
        List<BookEntity> bookEntityList2 = new ArrayList<>();
        bookEntityList1.add(bookEntity2_1);
        AuthorEntity authorEntity2 = new AuthorEntity("Michele Rech", "Zerocalcare", LocalDate.of(1983, 12, 12), bookEntityList2);
        authorEntity2.setCreatedAt( LocalDateTime.of(2018, Month.JULY, 29, 19, 30, 40));
        bookEntity2_1.setAuthor(authorEntity2);

        bookEntityList.add(bookEntity1_1);
        bookEntityList.add(bookEntity2_1);

        authorEntityList.add(authorEntity1);
        authorEntityList.add(authorEntity2);
    }

    public static List<BookEntity> listAllMockedBooks() {
        setUp();
        return bookEntityList;
    }

    public static List<AuthorEntity> listAllMockedAuthors() {
        setUp();
        return authorEntityList;
    }
}
