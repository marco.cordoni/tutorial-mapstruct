package dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.quarkus.runtime.annotations.RegisterForReflection;
import jakarta.json.bind.annotation.JsonbDateFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@RegisterForReflection
public class BookAndAuthorDto extends BaseDto {
    private Integer bookId;
    private String bookName;

    @JsonbDateFormat("yyyy-MM-dd")
    private LocalDate publicationDate;

    private String authorName;
    private String authorAlias;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;
}
