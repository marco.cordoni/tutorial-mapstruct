package dto;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@RegisterForReflection
public class AuthorSimplifiedDto extends BaseDto {
    private Integer id;
    private String nomeMappato;
    private String nameAndAlias;
    private String birthDate;
    private String birthDateFormatted;
}
