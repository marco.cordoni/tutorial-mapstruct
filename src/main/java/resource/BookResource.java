package resource;

import dto.BookAndAuthorDto;
import dto.BookDto;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import service.BookService;

import java.util.List;

@Path("/api/books")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class BookResource {

    @Inject
    private BookService bookService;

    @GET
    public Response findAll() {
        List<BookDto> bookDtoList = bookService.findAll();
        return Response.status(Response.Status.OK).entity(bookDtoList).build();
    }

    @GET
    @Path("/bookAndAuthor")
    public Response findAllBookAndAuthor() {
        List<BookAndAuthorDto> bookAndAuthorDtosList = bookService.findAllBookAndAuthor();
        return Response.status(Response.Status.OK).entity(bookAndAuthorDtosList).build();
    }

    @POST
    public Response saveBook(BookDto bookDtoToSave) {
        BookDto bookDtoSaved = bookService.saveBook(bookDtoToSave);
        return Response.status(Response.Status.CREATED).entity(bookDtoSaved).build();
    }
}
