package entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthorEntity extends BaseEntity {
    private String name;
    private String alias;
    private LocalDate birthDate;
    private List<BookEntity> books = new ArrayList<>();
}
