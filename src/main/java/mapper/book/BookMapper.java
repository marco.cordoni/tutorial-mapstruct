package mapper.book;

import dto.AuthorDto;
import dto.BookAndAuthorDto;
import dto.BookDto;
import entity.AuthorEntity;
import entity.BookEntity;
import mapper.UtilsMapper;
import mapper.author.AuthorMapper;
import org.mapstruct.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Mapper(
        componentModel = MappingConstants.ComponentModel.JAKARTA,
        uses = {UtilsMapper.class, AuthorMapper.class},
        unmappedTargetPolicy = ReportingPolicy.WARN
)
public abstract class BookMapper {

    // After the map method, this function is automatically applied on the target object
    // this works both with the single object BookDto and with the list of BookDto
    @AfterMapping
    protected void convertBookNameToUpperCase(@MappingTarget BookDto bookDto) {
        bookDto.setName(bookDto.getName().toUpperCase());
    }

    // ---------------------- MAPPING WITH CUSTOM MAPPING FOR 1 FIELD ----------------------
    @Named("mapBookEntityToDto") // To solve "Ambiguous mapping"
    @Mapping(source = "author", target = "author", qualifiedByName = "mapAuthorEntityToDtoWithoutBooks")
    public abstract BookDto map(BookEntity book);

    @Named("mapBookDtoToEntity")
    @Mapping(source = "author", target = "author", qualifiedByName = "mapAuthorDtoToEntity")
    public abstract BookEntity map(BookDto book);

    @Named("mapAuthorEntityToDto")
    @Mapping(source = "books", target = "books", qualifiedByName = "mapEntityListToDtoWithoutAuthor")
    public abstract AuthorDto map(AuthorEntity value);

    // ---------------------- MAPPING IGNORING USELESS FIELDS ----------------------

    @Named("mapBookEntityWithoutId") // To solve "Ambiguous mapping"
    @Mapping(source = "id", target = "id", ignore = true)
    public abstract BookDto mapBookEntityWithoutId(BookEntity book);

    // ---------------------- MAPPING FROM MULTIPLE SOURCES ----------------------

    @Named("mapBookEntityToBookAndAuthorDto")
    @Mapping(source = "book.id", target = "bookId") // can't map automatically
    @Mapping(source = "book.name", target = "bookName")
    @Mapping(source = "authorOfTheBook.name", target = "authorName")
    @Mapping(source = "authorOfTheBook.alias", target = "authorAlias")
    @Mapping(target = "createdAt", expression = "java(takeLatest(book.getCreatedAt(), authorOfTheBook.getCreatedAt()))") // FROM DIFFERENT FIELDS OF DIFFERENT OBJECTS
//    @Mapping(source = ".", target = "createdAt", qualifiedByName = "takeLatest")
//    @Mapping(source = "book.createdAt", target = "createdAt") // ERROR: Several possible source properties for target property createdAt
    @Mapping(source = "book.updatedAt", target = "updatedAt") // ERROR: Several possible source properties for target property updatedAt
    public abstract BookAndAuthorDto mapBookEntityToBookAndAuthorDto(BookEntity book, AuthorEntity authorOfTheBook);

    // ---------------------- MAPPING LIST USING A SPECIF MAPPER FOR EACH ELEMENT ----------------------
    @Named("mapEntityListToDtoList")
    @IterableMapping(qualifiedByName = "mapBookEntityToDto")
    public abstract List<BookDto> mapEntityListToDtoList(List<BookEntity> list);

    // We need @Named because there are 2 methods that take a List<BookEntity> list and this one is used by the method named mapAuthorEntityToDto
    @Named("mapEntityListToDtoWithoutAuthor")
    @IterableMapping(qualifiedByName = "mapBookEntityWithoutAuthor")
    public abstract List<BookDto> mapEntityListToDtoWithoutAuthor(List<BookEntity> list);

    // No need of @IterableMapping because there is only 1 method who take a BookDto and return a BookEntity
//    @IterableMapping(qualifiedByName = "mapBookDtoToEntity")
    public abstract  List<BookEntity> mapDtoListToEntityList(List<BookDto> list);


    // ---------------------- TOTALLY CUSTOM MAPPER ----------------------
    @Named("mapBookEntityToDtoOnlyName") 
    public BookDto mapBookEntityToDtoOnlyName(BookEntity book) {
        BookDto bookDto = new BookDto();
        bookDto.setName(book.getName());
        return bookDto;
    }

    @Named("takeLatest")
    public LocalDateTime takeLatest(LocalDateTime l1, LocalDateTime l2) {
        if (l1 == null && l2 != null) {
            return l2;
        }

        if (l1 != null && l2 == null) {
            return l1;
        }

        if (l1 == null && l2 == null) {
            return null;
        }

        return l1.isAfter(l2) ? l1 : l2;
    }
}
