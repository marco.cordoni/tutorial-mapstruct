package mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.Named;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA)
public abstract class UtilsMapper {

    @Named("setUnknownIfNull")
    public String setUnknownIfNull(String s) {
        return s != null ? s : "Unknown";
    }

    @Named("formatLocalDateToString")
    public String formatLocalDateToString(LocalDate localDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        return localDate.format(formatter);
    }

}
