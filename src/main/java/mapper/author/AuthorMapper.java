package mapper.author;

import dto.AuthorDto;
import dto.AuthorSimplifiedDto;
import dto.BookDto;
import entity.AuthorEntity;
import entity.BookEntity;
import mapper.UtilsMapper;
import org.mapstruct.*;

import java.util.List;

@Mapper(
        componentModel = MappingConstants.ComponentModel.JAKARTA,
        uses = {UtilsMapper.class},
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public abstract class AuthorMapper {

    // ---------------------- SIMPLE MAPPING ----------------------
    @Named("mapAuthorDtoToEntity")
    public abstract AuthorEntity map(AuthorDto value);

    @Named("mapAuthorEntityToDto")
    public abstract AuthorDto map(AuthorEntity value); // DO NOT USE THIS, IT HAS AN INFINITE RECURSION OF MAPPING BETWEEN BOOKS AND AUTHOR

    // ---------------------- MAPPING IGNORING USELESS FIELDS ----------------------
    @Named("mapAuthorEntityToDtoWithoutBooks")
    @Mapping(source = "books", target = "books", ignore = true)
    public abstract AuthorDto mapAuthorEntityToDtoWithoutBooks(AuthorEntity value);

    @Named("mapBookEntityWithoutAuthor")
    @Mapping(source = "author", target = "author", ignore = true)
    public abstract BookDto mapBookEntityWithoutAuthor(BookEntity book);

    // ---------------------- MAPPING USING A CUSTOM MAPPER FOR A CERTAIN FIELD ----------------------
    @Named("mapAuthorEntityToDtoSetUnknownAliasIfNull")
    @Mapping(source = "alias", target = "alias", qualifiedByName = "setUnknownIfNull")
    public abstract AuthorDto mapAuthorEntityToDtoSetUnknownAliasIfNull(AuthorEntity author);

    @Named("mapAuthorEntityToDtoWithBooks")
    @Mapping(source = "books", target = "books", qualifiedByName = "mapBookEntityWithoutAuthor") // IMPORTANT!! AVOID INFINITE RECURSION
    public abstract AuthorDto mapAuthorEntityToDtoWithBooks(AuthorEntity value);

    // ---------------------- MAPPING FIELDS WITH DIFFERENT NAME ----------------------

    @Named("mapAuthorEntityToSimplifiedDto")
    @Mapping(source = "name", target = "nomeMappato") // DIFFERENT FIELD NAME
//    @Mapping(source = "birthDate", target = "birthDate") // USELESS, EVEN IF THE TYPES ARE DIFFERENT IT CAN DO IT WITHOUT HELP
    @Mapping(source = "birthDate", target = "birthDateFormatted", qualifiedByName = "formatLocalDateToString")
    // INLINE EXPRESSION MAPPING
//    @Mapping(
//            target = "birthDateFormatted",
//            expression = "java(value.getName().toUpperCase() + \" - \" + (value.getAlias() == null ? \"unknown\" : value.getAlias().toUpperCase()))"
//    )
    @Mapping(source = ".", target = "nameAndAlias", qualifiedByName = "concatNameAndAlias")
    public abstract AuthorSimplifiedDto mapAuthorEntityToSimplifiedDto(AuthorEntity value);
//    public abstract AuthorSimplifiedDto map(AuthorEntity value); // ERROR: DUPLICATE METHOD

    // ---------------------- MAPPING LIST USING A SPECIF MAPPER FOR EACH ELEMENT ----------------------
    @Named("mapEntityListToDtoListWithoutBooks")
    @IterableMapping(qualifiedByName = "mapAuthorEntityToDtoWithoutBooks")
    public abstract List<AuthorDto> mapEntityListToDtoListWithoutBooks(List<AuthorEntity> list);

    @Named("mapEntityListToDtoListWithBooks")
    @IterableMapping(qualifiedByName = "mapAuthorEntityToDtoWithBooks")
    public abstract List<AuthorDto> mapEntityListToDtoListWithBooks(List<AuthorEntity> list);

    @Named("mapEntityListToSimplifiedDtoList")
    @IterableMapping(qualifiedByName = "mapAuthorEntityToSimplifiedDto")
    public abstract List<AuthorSimplifiedDto> mapEntityListToSimplifiedDtoList(List<AuthorEntity> list);


    // ---------------------- CUSTOM MAPPER ----------------------
    @Named("concatNameAndAlias")
    public String concatNameAndAlias(AuthorEntity author) {
        if (author.getAlias() == null || author.getAlias().isEmpty()) {
            return author.getName();
        }

        return author.getName() + " aka " + author.getAlias();
    }

}
